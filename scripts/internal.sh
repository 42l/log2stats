#!/bin/bash
cat $1 | sed -n "$5" | docker run --rm -i \
    --cpu-shares 400 \
    --memory="1G" \
    allinurl/goaccess:latest \
    -a -o $3 \
    --no-progress \
    --log-format=COMBINED \
    - > $4
