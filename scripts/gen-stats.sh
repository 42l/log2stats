#!/bin/bash
set -euo pipefail

# $1: input file name (log file)
# $2: week/month number
# $3: crawlers param (--ignore-crawlers, --crawlers-only or nothing)
# $4: output file name (stats file)
# $5: month filter for logs-rapports-monthly
# $6: service-specific params
# $7: service name

uncompress() {
    if [[ "$1" == *.gz ]]; then
        zcat $@
    else
        cat $@
    fi
}

uncompress $1 | sed -n "${5-}" | head -n -1 | docker run --rm -i \
    --cpu-shares 400 \
    --memory="1G" \
    -v local-stats-data:/stats-data:rw \
    allinurl/goaccess:latest \
    -a -o html \
    --html-report-title="$7 Statistics, $2" \
    --exclude-ip 45.155.171.210 \
    --exclude-ip 45.155.171.163 \
    --exclude-ip 185.216.27.142 \
    --exclude-ip 188.213.25.178 \
    --exclude-ip 172.17.0.0/12 \
    --max-items=50 \
    --anonymize-ip \
    --anonymize-level=2 \
    --no-progress \
    --no-query-string \
    $6 \
    $3 > $4

if [ "$4" != "/dev/stdout" ]; then
    # Broccoli
    rm -f $4.br
    brotli --rm $4

    # chown for NFS
    chown nobody:nogroup $4.br
fi
