#!/bin/bash

ABS_FOLDER=$(dirname $(realpath $0))

GOACCESS_PARAMS="--log-format=COMBINED \
    --ignore-panel=REQUESTS \
    --ignore-panel=REQUESTS_STATIC \
    --ignore-panel=NOT_FOUND \
    --ignore-panel=REFERRERS \
    --ignore-panel=REFERRING_SITES \
    --ignore-panel=KEYPHRASES \
    --ignore-panel=REMOTE_USER \
    --ignore-panel=GEO_LOCATION"

$ABS_FOLDER/gen-stats.sh "$@" "$GOACCESS_PARAMS" "Nitter Service"
